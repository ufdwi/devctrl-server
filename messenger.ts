#!/usr/bin/env node
"use strict";

import * as ioMod from "socket.io";
import * as http from "http";
import * as url from "url";
import * as mongo from "mongodb";
import * as querystring from "querystring";
import * as nodemailer from "nodemailer";
import {
    DCDataModel,
    IndexedDataSet,
    Control,
    ControlData,
    ControlUpdateData,
    UserSession,
    Endpoint,
    EndpointData,
    IEndpointStatus,
    ClientType,
    UserInfo,
    UserInfoData,
    IDCEndpointStatusUpdate,
    IDCDataRequest,
    IDCDataAdd,
    IDCDataDelete,
    IDCDataExchange
} from "@devctrl/common";
import { DCConfig } from "./config";
import {IDCStudentNameUpdate} from "../common/DCSerializable";


let debug = console.log;
let mongoDebug = console.log;

class Messenger {
    mongodb: mongo.Db;
    dataModel: DCDataModel;
    tablesLoaded = {};
    config : any;
    app: http.Server;
    io :SocketIO.Server;
    sessions: mongo.Collection;
    shortIds = {};  // An index of shortened GUIDs to use in log messages or elsewhere
    longIds = {};

    constructor() {}

    run(config: any) {
        this.dataModel = new DCDataModel();
        this.config = config;

        // Connect to mongodb
        let mongoConnStr = "mongodb://" + config.mongoHost + ":" + config.mongoPort
            + "/" + config.mongoDB;

        mongo.MongoClient.connect( mongoConnStr, (err, db) =>{
            debug("mongodb connected");
            this.mongodb = db;
            this.sessions = this.mongodb.collection("sessions");
            // setEndpointsDisconnected operated directly on the data store, call it before initializing data model
            this.setEndpointsDisconnected();
            this.initDataModel();

        });


    }


    addData(request: IDCDataAdd, fn: (data: IDCDataExchange) => any) {
        let resp = {
            _id : request._id,
            userInfo_id: request.userInfo_id,
            add : {}
        };
        let request_handled = false;

        for (let table in this.dataModel.types) {
            if (request.add[table]) {
                let col = this.mongodb.collection(table);
                resp.add[table] = {};

                // Add an array of documents
                let addDocs = [];
                for (let data of request.add[table]) {
                    // Sanitize data by creating objects and serializing them
                    try {
                        data._id = this.guid();
                        let obj = new this.dataModel.types[table](data._id, data);
                        let doc = obj.getDataObject();
                        addDocs.push(doc);
                    }
                    catch (e) {
                        let errmsg = "invalid data received: " + e.message;
                        debug(errmsg);
                        resp["error"] = errmsg;
                        fn(resp);
                        return;
                    }
                }

                col.insertMany(addDocs, (err, r) => {
                    if (err) {
                        mongoDebug(err.message);
                        resp["error"] = err.message;
                        fn(resp);
                        return;
                    }

                    mongoDebug(r.insertedCount + " records added to " + table);

                    resp.add[table] = {};
                    for (let doc of addDocs) {
                        resp.add[table][doc._id] = doc;
                    }
                    this.dataModel.loadData(resp);
                    this.io.emit('control-data', resp);
                    fn(resp);
                });


                request_handled = true;
                break;
            }
        }

        if (! request_handled) {
            let errmsg = "no valid data found in add request";
            debug(errmsg);
            resp["error"] = errmsg;
            fn(resp);
            return;
        }
    }

    adminAuth(req: http.IncomingMessage, response: http.ServerResponse, identifier: string) {
        if (! identifier) {
            this.errorResponse(response, 400, "Don't come here without a session");
            return;
        }

        let adminExpires = Date.now() + (1000 * 120); // 2 minutes from now
        let expDate = new Date(adminExpires);
        let expStr = expDate.toTimeString();

        this.debug({
            sessionId: identifier,
            msg: "admin expiration set to " + expStr
        });
        let loginExpires = parseInt(req.headers['oidc_claim_exp'] as string) * 1000;


        let session : UserSession = {
            login_expires: loginExpires,
            auth: true,
            admin_auth: true,
            admin_auth_expires: adminExpires,
            admin_auth_requested: false,
            username: req.headers["oidc_claim_preferred_username"] as string
        };


        this.sessions.findOneAndUpdate({ _id : identifier }, { '$set' : session },
            { returnOriginal: false},
            (err, r) => {
                if (err) {
                    this.errorResponse(response, 503, "mongodb error: " + err.message);
                    return;
                }

                if (r.value) {
                    this.debug({
                        sessionId: identifier,
                        msg: "auth_requested set to " + r.value.admin_auth_requested
                    });
                    response.setHeader('Content-Type', 'application/json');
                    response.writeHead(200);
                    response.end(JSON.stringify({ session: r.value}));
                }
                else {
                    this.errorResponse(response, 503, "failed to update user session");
                }
            }
        );
    }



    adminAuthCheck(socket : SocketIO.Socket) {
        let authCheckPromise = new Promise((resolve, reject) => {
            let session = socket["session"];

            //TODO: one of the auth granted log statements is not using the right client name
            if (session.admin_auth && session.admin_auth_expires > Date.now() || session.admin_auth_expires == -1) {
                this.debug({ sessionId: session._id, msg: `admin authorization granted (1) for ${session.client_name}`});
                resolve(true);
                return;
            }

            // Check for updated session info
            let col = this.mongodb.collection("sessions");

            col.findOne({_id : session._id}, (err, res) => {
                if (err) {
                    this.debug({
                        sessionId: session._id,
                        msg: `mongodb error during auth: ${err.message}`
                    });
                    reject({error: err.message});
                    return;
                }

                if (res) {
                    socket["session"] = res;

                    if (res.admin_auth && res.admin_auth_expires > Date.now() || res.admin_auth_expires == -1) {
                        this.debug({
                            sessionId: res._id,
                            msg: `admin authorization granted (2) for ${res.client_name}`
                        });
                        resolve(true);
                        return;
                    }

                    this.debug({
                        sessionId: session._id,
                        msg: `unauthorized update request from ${res.client_name})`
                    });
                    this.debug({
                        sessionId: session._id,
                        msg: `auth = ${res.admin_auth}, expires = ${res.admin_auth_expires}, now = ${Date.now()}`
                    });
                    reject({error: "unauthorized"});
                    return;
                }

                debug("authCheck: session no found " + session._id);
                reject({error: "session not found"});
            });
        });

        return authCheckPromise;
    }

    adminAuthCheckHttp(req: http.IncomingMessage, response: http.ServerResponse) {
        return new Promise((resolve, reject) => {
            let identifier = this.getIdentifier(req);

            if (! identifier) {
                reject({error: "adminAuthCheckHttp fail - no identifier"});
                return;
            }

            this.sessions.findOne({_id : identifier}, (err, res) => {
                if (err) {
                    let msg = `mongodb error during auth for ${identifier}: ${err.message}`;
                    reject(msg);
                    return;
                }

                if (res) {
                    if (res.admin_auth && res.admin_auth_expires > Date.now() || res.admin_auth_expires == -1) {
                        resolve(res);
                        return;
                    }

                    debug(`unauthorized update request from ${identifier} (${res.client_name})`);
                    debug(`auth = ${res.admin_auth}, expires = ${res.admin_auth_expires}, now = ${Date.now()}`);
                    reject("unauthorized");
                    return;
                }

                debug("authCheck: session no found " + identifier);
                reject("session not found");
            });
        });
    }

    authCheckHttp(req: http.IncomingMessage, response: http.ServerResponse) {
        return new Promise((resolve, reject) => {
            let identifier = this.getIdentifier(req);

            if (! identifier) {
                reject("adminAuthCheckHttp fail - no identifier");
                return;
            }

            this.sessions.findOne({_id : identifier}, (err, res) => {
                if (err) {
                    let msg = `mongodb error during auth for ${identifier}: ${err.message}`;
                    reject(msg);
                    return;
                }

                if (res) {
                    if (res.auth) {
                        resolve(res);
                        return;
                    }

                    debug(`unauthorized request from ${identifier} (${res.client_name})`);
                    debug(`auth = ${res.auth}`);
                    reject("unauthorized");
                    return;
                }

                debug("authCheck: session no found " + identifier);
                reject("session not found");
            });
        });
    }


    broadcastControlValues(updates: ControlUpdateData[], fn: any, socket : SocketIO.Socket) {
        let controlsCollection = this.mongodb.collection(Control.tableStr);
        let controls = this.dataModel.tables[Control.tableStr] as IndexedDataSet<Control>;


        // Commit value to database for non-ephemeral controls
        for (let update of updates) {
            //debug(`control update received: ${update.control_id} = ${update.value}`);
            if (! controls[update.control_id]) {
                debug(`dropping update of invalid control_id ${update.control_id} from ${socket["session"].client_name}`);
                return;
            }
            if (update.status == "observed" && ! update.ephemeral) {
                controlsCollection.updateOne({ _id: update.control_id},
                    { '$set' : { value: update.value}},
                    (err, r) => {
                        if (err) {
                            mongoDebug(`control value update error: ${err.message}`);
                            return;
                        }

                        // update the data model
                        controls[update.control_id].value = update.value;
                    });
            }
        }

        this.io.emit('control-updates', updates);
    }

    createEndpointSession(req: http.IncomingMessage, response: http.ServerResponse) {
        let  identifier = (new mongo.ObjectID()).toString();

        let parts = url.parse(req.url);
        let clientName = parts.query;

        //  Create a new UserInfo object
        let uiData : UserInfoData = {
            _id : "0",
            name : clientName,
            clientType: ClientType.ncontrol
        };

        let uiDataEx = {
            _id: (new mongo.ObjectID()).toString(),
            userInfo_id: "createEndpointSession",
            add : {
                userInfo : [uiData]
            }
        };

        this.addData( uiDataEx, (data) => {
            let newId = Object.keys(data.add.userInfo)[0];

            // New session
            let session : UserSession = {
                _id: identifier,
                login_expires: 0,
                auth: true,
                admin_auth: true,
                admin_auth_expires: -1,
                userInfo_id: newId
            };


            if (req.headers["oidc_claim_preferred_username"]) {
                session.username = req.headers["oidc_claim_preferred_username"] as string;
            }

            this.sessions.insertOne(session, (err, result) => {
                if (err) {
                    this.errorResponse(response, 503, "mongodb error: " + err.message);
                    return;
                }

                response.writeHead(200);
                response.end(JSON.stringify({ session: session}));
            });
        });
    }

    debug(obj: any) {
        if (typeof obj == "string") {
            console.log(obj);
        }

        let msg = "";
        if (obj.sessionId) {
            msg += "s:" + this.shortId(obj.sessionId) + " ";
        }

        msg += obj.msg;
        console.log(msg);
    }


    deleteData(data: IDCDataExchange, fn: (data: IDCDataExchange) => void) {
        let resp = {
            _id: data._id,
            userInfo_id: data.userInfo_id,
        };


        if (!data.del ||
            !data.del.table ||
            !data.del._id ||
            !this.dataModel.types[data.del.table])
        {
            let errmsg = `deleteData: invalid table name`;
            if (data.del && data.del.table) {
                errmsg += " " + data.del.table;
            }
            debug(errmsg);
            resp["error"] = errmsg;
            fn(resp);
            return;
        }

        debug(`delete ${data.del._id} from ${data.del.table}`);
        this.dataModel.loadData(data);
        resp["del"] = data.del;
        fn(resp);
        this.io.emit("control-data", resp);

        // Write update to DB
        let col = this.mongodb.collection(data.del.table);
        col.deleteOne({ _id : data.del._id }, (err, res) => {
            if (err) {
                let errmsg = "deleteData: mongo error: " + err.message;
                debug(errmsg);
                return;
            }

            if (res.result.n == 0) {
                let errmsg = "deleteData: doc not found: " + data.del._id;
                debug(errmsg);
                return;
            }
        });

    }


    disconnectSocket(socket : SocketIO.Socket) {
        if (socket["endpoint_id"]) {
            let _id = socket["endpoint_id"];

            debug(`client disconnected: setting endpoint ${_id} to offline`);
            let col = this.mongodb.collection(Endpoint.tableStr);

            col.updateOne({ _id : _id },
                { '$set' :
                    {
                        "epStatus.messengerConnected" : false,
                        "epStatus.reachable": false,
                        "epStatus.connected": false,
                        "epStatus.loggedIn": false,
                        "epStatus.polling": false,
                        "epStatus.responsive": false,
                        "epStatus.ok": false
                    }
                },
                (err, r) => {
                    if (err) {
                        mongoDebug(`update { request.table } error: { err.message }`);
                        return;
                    }

                    // Get the updated object and broadcast the changes.
                    let table = {};
                    col.findOne({_id: _id}, (err, doc) => {
                        if (err) {
                            mongoDebug(`update { request.table } error: { err.message }`);
                            return;
                        }

                        table[doc._id] = doc;
                        let data = {
                            _id: this.guid(),
                            userInfo_id: _id,
                            add: {}
                        };
                        data.add[Endpoint.tableStr] = table;
                        this.dataModel.loadData(data);
                        this.io.emit('control-data', data);
                    });
                }
            );
        }
        else {
            let session = socket["session"];
            if (session) {
                debug(`client ${session._id} (${session.client_name} disconnected`);
            }
            else {
                debug("unidentified client disconnected");
            }
        }
    }

    doLogon(req: http.IncomingMessage, response: http.ServerResponse, identifier: string) {
        if (! identifier) {
            this.errorResponse(response, 400, "Don't come here without a session");
            return;
        }

        let parts = url.parse(req.url);
        let queryVars = querystring.parse(parts.query);

        let admin_auth_requested = false;
        if (queryVars['admin_auth_requested']) {
            admin_auth_requested = true;
        }

        this.debug({sessionId: identifier, msg: "do_logon, admin_auth_requested = " + admin_auth_requested});

        //TODO: This is dependency on mod_auth_openidc for authentication
        //let loginExpires = parseInt(req.headers['oidc_claim_exp'] as string) * 1000;
        let nowDate = new Date();
        // set this to an hour from now until we figure out something better
        let loginExpires = Date.now() + (60 * 1000 * 60);
        let loginExpiresTime = new Date(loginExpires);

        let nowTimeStr = nowDate.toTimeString().substr(0, 17);
        this.debug({
            sessionId: identifier,
            msg: `loginExpires: ${loginExpiresTime.toTimeString().substr(0, 17)}, Now: ${nowTimeStr}`
        });

        this.sessions.findOneAndUpdate({ _id: identifier},
            { '$set' : {
                login_expires : loginExpires,
                auth: true,
                admin_auth_requested: admin_auth_requested,
                username: req.headers["oidc_claim_preferred_username"]
            }},
            (err, doc) => {
                if (err) {
                    this.errorResponse(response, 503, "mongodb error: " + err.message);
                    return;
                }

                // Redirect to the location specified in the query string
                if (doc.value) {
                    let location = '/';
                    if (queryVars.location) {
                        location = queryVars.location as string;
                    }

                    this.debug({
                        sessionId: identifier,
                        msg: `logon complete, redirecting to ${location}, aar=${admin_auth_requested}`
                    });

                    response.setHeader('Location', location);
                    response.writeHead(302);
                    response.end();
                }
                else {
                    // No session found
                    this.errorResponse(response, 400, "Session not found for identifier " + identifier);
                }
            }
        );
    }


    errorResponse(res: http.ServerResponse, code: number, message: string) {
        res.writeHead(code);
        res.end(message);

        debug(`error: ${message}`);
    }


    getData(request: IDCDataRequest, fn: (data: IDCDataExchange) => void, session : UserSession) {
        this.debug({ sessionId: session._id, msg: "data requested from " + request.table});
        let col = this.mongodb.collection(request.table);

        let resp = {
            _id: request._id,
            userInfo_id: "messenger",
            add : {}
        };


        //TODO: the dataModel and the database are getting out of sync. Related to rapid updates being processed out of order
        if (JSON.stringify(request.params) == "{}") {
            // Just take the whole table from the data model
            resp.add[request.table] = this.dataModel.getTableSerialized(request.table);
            fn(resp);
        }
        else {
            let table = {};
            col.find(request.params).forEach(
                (doc) => {
                    table[doc._id] = doc;
                },
                () => {
                    resp.add[request.table] = table;
                    fn(resp);
                }
            );
        }
    }

    getIdentifier(req: http.IncomingMessage) : string {
        let cookies = {};
        let cookieStr = req.headers['cookie'] as string;
        if (cookieStr) {
            let cstrs = cookieStr.split(';');

            for (let cstr of cstrs) {
                let [name, value] = cstr.split("=");
                cookies[name.trim()] = value;
            }
        }

        if (cookies[this.config.identifierName]) {
            return cookies[this.config.identifierName];
        }

        if (req.headers['ncontrol-auth-id']) {
            return <string>req.headers['ncontrol-auth-id'];
        }

        return '';
    }

    getDataExchangeId(req: http.IncomingMessage) {
        return <string>req.headers["data-exchange-id"];
    }

    guid() : string {
        return (new mongo.ObjectID()).toString();
    }




    httpHandler(req: http.IncomingMessage, res: http.ServerResponse) {
        let identifier = this.getIdentifier(req);

        let parts = url.parse(req.url);
        let pathComponents = parts.pathname.split("/");

        this.debug({
            sessionId: identifier,
            msg: `http request: ${parts.pathname}`
        });

        if (pathComponents[0] == '') {
            pathComponents= pathComponents.slice(1);
        }

        let [api, endpoint, ...path] = pathComponents;

        //TODO: implement REST API for all data functions
        if (api == "api") {
            if (endpoint == "data") {
                if (req.method == 'DELETE') {
                    this.adminAuthCheckHttp(req, res)
                        .then((session : UserSession) => {
                            let [table, _id] = path;

                            this.deleteData({
                                    _id: this.getDataExchangeId(req),
                                    userInfo_id: session.userInfo_id,
                                    del:    { table: table, _id: _id}
                                },
                                (data) => {
                                    if (data.error) {
                                        res.writeHead(400);
                                        res.end(JSON.stringify(data));
                                        return;
                                    }

                                    res.writeHead(200);
                                    res.end(JSON.stringify(data));
                                    return;
                                });
                        })
                        .catch((err) => {
                            this.errorResponse(res, 400, err);
                        });

                    return;
                }
                else if (req.method == 'GET') {
                    this.authCheckHttp(req, res)
                        .then((session : UserSession) => {
                            let [table, _id] = path;

                            this.getData({
                                    _id: this.getDataExchangeId(req),
                                    table: table,
                                    params: { _id: _id }
                                },
                                (data) => {
                                    if (data.error) {
                                        res.writeHead(400);
                                        res.end(JSON.stringify(data));
                                        return;
                                    }

                                    res.writeHead(200);
                                    res.end(JSON.stringify(data));
                                    return;
                                },
                                session);
                        })
                        .catch((err) => {
                            this.errorResponse(res, 400, err);
                        });

                    return;
                }
            }
            else if (endpoint == "test") {
                res.writeHead(200);
                res.end("yeah, I'm alive");
                return;
            }
            else {
                res.writeHead(400);
                res.end("api endpoint not recognized");
                return;
            }
        }
        else if (api == "auth") {



            if (endpoint == 'admin') {
                /**
                 * admin location should be protected by the proxying webserver
                 * admin authorization will be granted to any user who hits the location
                 */
                if (path[0] == 'get_auth') {
                    this.adminAuth(req, res, identifier);
                    return;
                }
                else if (path[0] == 'create_endpoint_session') {
                    /**
                     * https://devctrl.dwi.ufl.edu/auth/admin/create_endpoint_session?endpoint-name
                     */
                    this.createEndpointSession(req, res);
                    return;
                }
            }
            else if (endpoint == 'do_logon') {
                /**
                 * This endpoint should be protected.  Only users authorized to access the application
                 * should be able to access it
                 */
                this.doLogon(req, res, identifier);
                return;
            }
            else if (endpoint == 'user_session') {
                this.userSession(req, res, identifier);
                return;
            }
            else if (endpoint == 'revoke_admin') {
                this.revokeAdminAuth(req, res, identifier);
                return;
            }
        }
        else {
            res.writeHead(200);
            res.end("API missed");
            return;
        }

        debug(`api call to ${req.url} unhandled`);
        res.writeHead(500);
        res.end("unhandled api call");
    }

    /*
     * Check to see if all data has been loaded
    */
    initCheckTablesLoaded() {
        for (let t in this.dataModel.tables) {
            if (! this.tablesLoaded[t]) {
                // nope, not ready yet
                return;
            }
        }

        // All data loaded, proceed to next startup phase
        this.initServers();
    }

    initDataModel() {
        for (let t in this.dataModel.tables) {
            let col = this.mongodb.collection(t);
            let table = {};
            let data = {
                _id: "messenger_init",
                userInfo_id: "messenger_init",
                add: {}
            };
            col.find().forEach(
                (doc) => {
                    table[doc._id] = doc;
                },
                () => {
                    data.add[t] = table;
                    this.dataModel.loadData(data);
                    this.tablesLoaded[t] = true;
                    this.initCheckTablesLoaded();
                }
            );
        }
    }



    initServers() {
        this.app = http.createServer((req, response) => {
            this.httpHandler(req, response);
        });


        this.app.listen(config.ioPort, "localhost");
        this.io = ioMod(this.app, { path: config.ioPath});
        debug(`socket.io listening at ${config.ioPath} on port ${config.ioPort}`);
        this.io.on('connection', (socket) => {
            this.ioConnection(socket);
        });

        // Set authorization function
        this.io.use((socket,next) => {
            let id = this.socketAuthId(socket);
            //debug("authenticating user " + id);
            if (id) {
                this.sessions.findOne({_id : id}, (err, res) => {
                    if (err) {
                        this.debug({ sessionId: id,
                            msg: `mongodb error during auth: ${err.message}`});
                        next(new Error("Authentication error"));
                        return;
                    }

                    if (res && res.auth) {
                        this.debug({ sessionId: id, msg: `user authenticated on socket ${socket.id}`});
                        socket["session"] = res;
                        next();
                        return;
                    }

                    this.debug({ sessionId: id, msg: "connection attempt from unauthorized user"});
                    next(new Error("Unauthorized"));
                });
            }
            else {
                this.debug("no auth id supplied");
                next(new Error("Authentication required"));
            }
        });
    }


    ioConnection(socket: SocketIO.Socket) {
        let clientIp = socket.request.connection.remoteAddress;
        if (socket.request.headers['x-forwarded-for']) {
            clientIp = socket.request.headers['x-forwarded-for'];
        }

        let session = socket["session"];

        this.debug({ sessionId: session._id, msg: 'a user connected from ' + clientIp});
        socket.emit("messenger-data", { version: this.config.version });


        socket.on('get-data', (req, fn) => {
            this.getData(req, fn, session);
        });
        socket.on('add-data', (req, fn) => {
            console.log("add-data request");
            this.adminAuthCheck(socket)
                .then(() => {
                    this.addData(req, fn);
                })
                .catch((msg) => { fn(msg) });
        });
        socket.on('update-data', (req, fn) => {
            this.adminAuthCheck(socket)
                .then(() => {
                    this.updateData(req, fn, session);
                })
                .catch((msg) => { fn(msg) });
        });
        socket.on('control-updates', (req, fn) => {
            this.broadcastControlValues(req, fn, socket);
        });
        socket.on('register-endpoint', (req, fn) => {
            this.adminAuthCheck(socket)
                .then(() => {
                    this.registerEndpoint(req, fn, socket);
                })
                .catch((msg) => { fn(msg) });
        });
        socket.on('sos', (req, fn) => { this.sos(req, fn, session); });
        socket.on('update-endpoint-enabled', (req,fn) => {
            this.updateEndpointEnabled(req, fn, session);
        });
        socket.on('update-student-name-config', (req, fn) => {
            this.updateStudentNameConfig(req, fn, session);
        });


        socket.on('disconnect', () => {
            this.disconnectSocket(socket);
        });
    }

    /**
     * Register endpoint associates and endpoint id with the socket held by
     * the ncontrol instance communicating with that endpoint.  If the socket
     * is disconnected, we can update the endpoint status accordingly.
     *
     * @param fn
     * @param socket SocketIO.Socket the socket this message was sent on
     */

    registerEndpoint(request, fn : any, socket : SocketIO.Socket) {
        if (! request.endpoint_id) {
            debug('register endpoint: endpoint_id not specified');
            fn({ error: "endpoint_id not specified"});
            return;
        }

        debug(`endpoint ${request.endpoint_id} registered on socket ${socket.id}`);
        socket["endpoint_id"] = request.endpoint_id;

        fn({userInfo_id: socket['session'].userInfo_id});
    }


    revokeAdminAuth(req: http.IncomingMessage, response: http.ServerResponse, identifier: string) {
        // This endpoint sets removes the admin auth from the session
        // Additional actions will be required to ensure that users cannot regain admin auth
        // without resupplying credentials
        if (! identifier) {
            this.errorResponse(response, 400, "Don't come here without a session");
            return;
        }

        let adminExpires = Date.now(); // now
        let loginExpires = parseInt(req.headers['oidc_claim_exp'] as string) * 1000;


        let session : UserSession = {
            login_expires: loginExpires,
            auth: true,
            admin_auth: false,
            admin_auth_expires: adminExpires,
            admin_auth_requested: false,
            username: req.headers["oidc_claim_preferred_username"] as string
        };


        this.sessions.findOneAndUpdate({ _id : identifier }, { '$set' : session },
            { returnOriginal: false},
            (err, r) => {
                if (err) {
                    this.errorResponse(response, 503, "mongodb error: " + err.message);
                    return;
                }

                if (r.value) {
                    debug("auth_requested set to " + r.value.admin_auth_requested);
                    response.setHeader('Content-Type', 'application/json');
                    response.writeHead(200);
                    response.end(JSON.stringify({ session: r.value}));
                }
                else {
                    this.errorResponse(response, 503, "failed to update user session");
                }
            }
        );
    }

    /**
     * At startup, set all endpoints to a disconnected status
     */

    setEndpointsDisconnected() {
        let col = this.mongodb.collection(Endpoint.tableStr);
        return col.updateMany({}, { '$set' : {
            "epStatus.messengerConnected" : false,
            "epStatus.reachable": false,
            "epStatus.connected": false,
            "epStatus.loggedIn": false,
            "epStatus.polling": false,
            "epStatus.responsive": false,
            "epStatus.ok": false
        }});
    }

    /**
     * Find and return a substring of a GUID that is unique to this application context.  Warning: these values
     * may not be unique or consistent across restarts of this program
     * @param {string} id
     * @return {string} short id
     */
    shortId(id : string) {
        if (this.shortIds[id]) {
            return this.shortIds[id];
        }

        let fullLen = id.length;
        let i = 5;

        while (i < fullLen) {
            let short = id.substr(0, i);

            if (! this.longIds[short]) {
                this.longIds[short] = id;
                this.shortIds[id] = short;
                return short;
            }

            i++;
        }

        throw new Error("error identifying shortId. check the algorithm, cuz this shouldn't happen");
    }


    socketAuthId(socket) {

        if (socket.handshake && socket.handshake.headers['cookie']) {
            let cookies = {};

            let cstrs = socket.handshake.headers['cookie'].split(';');

            for (let cstr of cstrs) {
                let [name, value] = cstr.split("=");
                cookies[name.trim()] = value;
            }


            if (cookies[this.config.identifierName]) {
                return cookies[this.config.identifierName];
            }
        }

        if (socket.handshake && socket.handshake.headers['ncontrol-auth-id']) {
            return socket.handshake.headers['ncontrol-auth-id'];
        }

        return '';
    }

    sos(req, fn, session: UserSession) {
        let transporter = nodemailer.createTransport({
            host: 'smtp.ufl.edu',
            port: 25,
            name: 'controller.dwi.ufl.edu'
        });


        let userInfo = this.dataModel.getItem(session.userInfo_id, UserInfo.tableStr);
        let clientName = userInfo.name;

        let destList = this.config.sosEmails.join(",");

        let message = {
            from: 'devctrl@controller.dwi.ufl.edu',
            to: destList,
            subject: `SOS from ${clientName}`,
            text: `An SOS message has been received from DevCtrl client ${clientName}`
        };

        transporter.sendMail(message, (err, info) => {
            if (err) {
                this.debug({sessionId: session._id, msg: `error sending SOS email: ${err}`});
                return;
            }

            this.debug({sessionId: session._id, msg: `SOS email sent`});

        });
    }


    updateData(request: IDCDataExchange, fn: any, session: UserSession) {
        if (! request.add) {
            this.debug({ sessionId: session._id, msg: "invalid update request received"});
            fn({error: "invalid update request"});
            return;
        }

        if (request.del) {
            this.debug({ sessionId: session._id, msg: "delete request directed to updateData"});
            fn({error: "invalid update request. delete may not be specified"});
            return;
        }

        for (let t in request.add) {
            if (! this.dataModel.tables[t]) {
                debug(`invalid table update: ${t}`);
                fn({error: `invalid table ${t}`});
                return;
            }

            for (let id in request.add[t]) {
                if (! this.dataModel.tables[t][id]) {
                    let msg = `update request for non-existent item ${t}.${id}`;
                    this.debug({ sessionId: session._id, msg: msg});
                    fn({error: msg});
                    return;
                }
            }
        }

        try {
            this.dataModel.loadData(request);
        }
        catch (e) {
            this.debug({ sessionId: session._id, msg: "update data error: " + e.message});
            fn({error: e});
            return;
        }

        this.io.emit('control-data', request);
        fn(request);

        // Due to prior issues with rapid updates being applied out of order, we now update the data model,
        // then broadcast updates, and finally write data back to the data store
      //TODO: this fix didn't work.  endpoint status is still getting out of sync
        for (let t in request.add) {
            let col = this.mongodb.collection(t);

            for (let id in request.add[t]) {
                let obj = this.dataModel.getItem(id, t).getDataObject();
                delete obj._id;
                col.updateOne({ _id : id}, { '$set' : obj}, (err, r) => {
                    if (err) {
                        debug(`update ${t} error: ${err.message}`);
                        fn({error: err.message});
                        return;
                    }
                });
            }
        }
    }

    updateEndpointEnabled(request: IDCEndpointStatusUpdate, fn: any, session: UserSession) {

        if (! this.dataModel.tables[Endpoint.tableStr][request.endpoint_id]) {
            debug(`Update endpointEnabled for invalid endpoint: ${request.endpoint_id}`);
            fn({error: `Update endpointEnabled for invalid endpoint: ${request.endpoint_id}`});
            return;
        }

        this.debug({
            sessionId: session._id,
            msg: `update endpoint enabled: ${request.endpoint_id}:${request.status.enabled}`
        });
        let ep = <EndpointData>this.dataModel.getItem(request.endpoint_id, Endpoint.tableStr).getDataObject();
        ep.epStatus.enabled = request.status.enabled;


        let updateData = {
            _id: request._id,
            userInfo_id: request.userInfo_id,
            add: {endpoints: {}}
        };

        updateData.add.endpoints[request.endpoint_id] = ep;

        this.updateData(updateData, fn, session);
    }

    updateStudentNameConfig(request:IDCStudentNameUpdate, fn: any, session: UserSession) {
        //TODO: this is a lot of code for a very specific operation
        this.debug({
            sessionId: session._id,
            msg: `update student name config: ${request.control_id}:${request.name}`
        });

        let cData = <ControlData>this.dataModel.getItem(request.control_id, Control.tableStr).getDataObject();

        if (! cData.config["names"]) cData.config["names"] = {};
        if (! cData.config["names"][request.course]) cData.config["names"][request.course] = {};
        if (! cData.config["names"][request.course][request.section])
            cData.config["names"][request.course][request.section] = {};

        cData.config["names"][request.course][request.section][request.seat] = request.name;
        let updateData = {
            _id: request._id,
            userInfo_id: session.userInfo_id,
            add: { controls: {}}
        };

        updateData.add.controls[request.control_id] = cData;

        this.updateData(updateData, fn, session);
    }


    userSession(req: http.IncomingMessage, response: http.ServerResponse, identifier: string) {
        // This endpoint returns the current user session or creates a new one if necessary
        let idProvided = true;

        if (! identifier) {
            idProvided = false;
            // This identifier is secret and known only to this client
            identifier = this.guid();
        }

        if (idProvided) {
            this.sessions.findOne({ _id: identifier}, (err, doc) => {
                if (err) {
                    this.errorResponse(response, 503, "mongodb error: " + err.message);
                    return;
                }

                if (doc) {
                    let session:UserSession = (<UserSession>doc);

                    // Create UserInfo object if necessary
                    if (! session.userInfo_id) {
                        let uiData : UserInfoData = {
                            _id : this.guid(),
                            name: session.client_name,
                            clientType: ClientType.web
                        };

                        let uiDataEx = {
                            _id: this.guid(),
                            userInfo_id: uiData._id,
                            add: {
                                userInfo: [uiData]
                            }
                        };

                        this.addData(uiDataEx, (data) => {
                            let newId = Object.keys(data.add.userInfo)[0];
                            session.userInfo_id = newId;

                            this.sessions.updateOne({ _id: session._id },
                                { '$set' : { userInfo_id : newId}},
                                (err, doc) => {
                                    if (err) {
                                        this.errorResponse(response, 503, "mongodb error updating UserSession:" + err.message);
                                        return;
                                    }

                                    response.writeHead(200);
                                    response.end(JSON.stringify({session: session}));
                                });
                        });
                    }
                    else {
                        response.writeHead(200);
                        response.end(JSON.stringify({session: session}));
                    }
                }
                else {
                    // No session found
                    this.errorResponse(response, 400, "Session not found for identifier " + identifier);
                }
            });
        }
        else {
            //  Create a new UserInfo object
            let uiData : UserInfoData = {
                _id : this.guid(),
                name : "unknown client",
                clientType: ClientType.web
            };

            let uiDataEx = {
                _id: this.guid(),
                userInfo_id: uiData._id,
                add: { userInfo: [uiData]}
            };

            if (req.headers['x-forwarded-for']) {
                uiData.name = req.headers['x-forwarded-for'] as string;
            }

            this.addData(uiDataEx, (data) => {
                let newId = uiData._id;

                // New session
                let session : UserSession = {
                    _id: identifier,
                    login_expires: 0,
                    auth: false,
                    admin_auth: false,
                    admin_auth_expires: 0,
                    userInfo_id: newId
                };


                if (req.headers["oidc_claim_preferred_username"]) {
                    session.username = req.headers["oidc_claim_preferred_username"] as string;
                }

                this.sessions.insertOne(session, (err, _result) => {
                    if (err) {
                        this.errorResponse(response, 503, "mongodb error: " + err.message);
                        return;
                    }

                    let oneYear = 1000 * 3600 * 24 * 365;
                    let cookieExpire = new Date(Date.now() + oneYear);
                    let expStr = cookieExpire.toUTCString();
                    let idCookie = `${this.config.identifierName}=${identifier};expires=${expStr};path=/`;

                    response.setHeader('Set-Cookie', [idCookie]);
                    response.writeHead(200);
                    response.end(JSON.stringify({ session: session}));
                });

            });
        }
    }
}

let config = new DCConfig("messenger");
let messenger = new Messenger();
messenger.run(config);